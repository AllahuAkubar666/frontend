This is a very basic front-end for the task given to me from Buzly Labs Ltd.

**NOTE!**: Please make sure you run the backend server on http://localhost:3000
or change the BASE_URL constant in app/common/url.js file

Information: The Git strategy used for both projects was [Git Trunk](https://www.toptal.com/software/trunk-based-development-git-flow), which is perfect
for CI/CD.

Additional information:
Neither the front-end nor the back-end are perfect in any way.
There are a lot of things to consider and add such as:

- convert the app to be isomorphic/unversal react app
- apply the principles of progressive enhancement
- make the design responsive
- handle the additional error cases that are not covered
- create tests!
- improve the naming of the variables and functions
- maybe change the pagination strategy
- imporve the responses of the back-end!
- imporve all the configurations of webpack + utils

I really hope that you, if not enjoy, at least not feel repulsed by the code.

I promise I can do better!
