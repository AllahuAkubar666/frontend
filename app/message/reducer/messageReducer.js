import { MESSAGE_LOADED, MESSAGE_ERROR, START_LOADING } from '../actions/messageActions';

const messageReducer = (state = { message: undefined, error: undefined, loading: true }, action) => {
    switch (action.type) {
        case MESSAGE_LOADED:
            return { 
                message: action.payload, 
                error: undefined,
                loading: false
            };
        case MESSAGE_ERROR:
            return { 
                message: state.message, 
                error: action.payload,
                loading: false 
            };
        case START_LOADING:
            return {
                message: state.message,
                error: action.payload,
                loading: true
            }
        default:
            return state;
    }
};

export default messageReducer;
