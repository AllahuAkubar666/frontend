import { put, takeLatest } from 'redux-saga/effects';

import { messageLoaded, messageError } from '../actionCreatros/messageActionCreators';
import { LOAD_MESSAGE } from '../actions/messageActions';
import { BASE_URL } from '../../common/url';

function* loadMessage(action) {
    const { messageId } = action.payload;
    const url = new URL(`${BASE_URL}/message/${messageId}`);
    const message = yield fetch(url)
        .then(response => response.json());
    if (message.error) {
        yield put(messageError(message.error));
    } else {
        yield put(messageLoaded(message));
    }

}

function* message() {
    yield takeLatest(LOAD_MESSAGE, loadMessage);
}

export default message;
