import { LOAD_MESSAGE, MESSAGE_LOADED, MESSAGE_ERROR, START_LOADING } from '../actions/messageActions';

export const loadMessage = messageId => ({
    type: LOAD_MESSAGE,
    payload: {
        messageId
    }
});

export const messageLoaded = message => ({
    type: MESSAGE_LOADED,
    payload: message
});

export const messageError = error => ({
    type: MESSAGE_ERROR,
    payload: error
});

export const startLoading = () => ({
    type: START_LOADING
})