import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { loadMessage } from '../actionCreatros/messageActionCreators';

export const Message = props => {
    const history = useHistory();
    useEffect(() => {
        props.dispatch(loadMessage(props.match.params.messageId));
        props.dispatch({ type: 'START_LOADING' });
    }, []);
    return (
        <div className="mesage-page">
            {props.loading ? <div className="loader"></div> :
                <>
                    { props.error && <div>{props.error}</div>}
                    {props.message && <div className="individual-message">{props.message.text}</div>}
                    <div className="normal-button" onClick={() => history.push('/')}>Back</div>
                </>
            }
        </div>
    );
}

const mapStateToProps = state => {
    return {
        message: state.message.message,
        error: state.message.error,
        loading: state.message.loading
    };
};

export default connect(mapStateToProps)(Message);