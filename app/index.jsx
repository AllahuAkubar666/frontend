import 'normalize.css';
import 'react-notifications-component/dist/theme.css'
import './main.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import ReactNotification from 'react-notifications-component'

import Message from './message/components/Message';
import Messages from './messages/components/Messages';
import AddMessage from './addMessage/components/AddMessage';
import store from './store/store';


ReactDOM.render(
    <Provider store={store}>
        <ReactNotification />
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Messages} />
                <Route path="/message/:messageId" component={Message} />
                <Route path="/add-message" component={AddMessage} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app'));
