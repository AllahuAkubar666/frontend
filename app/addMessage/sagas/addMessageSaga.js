import { put, takeEvery } from 'redux-saga/effects';
import { store } from 'react-notifications-component';

import { messageAdded, messageAddError } from '../actionCreators/addMessageActionCreators';
import { ADD_MESSAGE } from '../actions/addMessageActions';
import { BASE_URL } from '../../common/url';

function* newMessage(action) {
    const { text } = action.payload;
    const url = new URL(`${BASE_URL}/message`);
    const message = yield fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ text })
    }).then(response => response.json());
    if (message.error) {
        yield put(messageAddError(message.error));
        store.addNotification({
            title: 'Could not add message!',
            message: text,
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
                duration: 1000,
                onScreen: true
            }
        });
    } else {
        yield put(messageAdded(message));
        store.addNotification({
            title: 'Message added',
            message: text,
            type: 'success',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
                duration: 1000,
                onScreen: true
            }
        });
    }
}

function* addMessage() {
    yield takeEvery(ADD_MESSAGE, newMessage);
}

export default addMessage;
