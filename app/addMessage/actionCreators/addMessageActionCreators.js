import { MESSAGE_ADDED, MESSAGE_ADD_ERROR, ADD_MESSAGE} from '../actions/addMessageActions';

export const messageAdded = message => ({ 
    type: MESSAGE_ADDED,
    payload: message 
});

export const messageAddError = error => ({ 
    type: MESSAGE_ADD_ERROR, 
    payload: error 
});

export const addMessage = text => ({ 
    type: ADD_MESSAGE,
    payload: { text } 
});
