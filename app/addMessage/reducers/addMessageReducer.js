import { MESSAGE_ADDED, MESSAGE_ADD_ERROR } from '../actions/addMessageActions';

const addMessageReducer = (state = { messsage: undefined, error: undefined }, action) => {
    switch (action.type) {
        case MESSAGE_ADDED:
            return { message: action.payload, error: undefined };
        case MESSAGE_ADD_ERROR:
            return { message: state.message, error: action.payload }
        default:
            return state;
    }
}

export default addMessageReducer;
