import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { addMessage } from '../actionCreators/addMessageActionCreators';

const AddMessage = props => {
    const [messageText, setMessageText] = useState('');
    const history = useHistory();
    const returnToHomePage = () => history.push('/');

    return (
        <div className="add-message-page">
            <textarea placeholder="Add message text" value={messageText} onChange={e => setMessageText(e.target.value)}></textarea>
            <div className="nav-buttons">
                <div className="cancel-button" onClick={returnToHomePage}>Cancel</div>
                <div className="normal-button" style={{ marginLeft: '1rem' }} onClick={() => {
                    props.dispatch(addMessage(messageText));
                    returnToHomePage();
                }}>Add</div>
            </div>
        </div>
    );
}

export default connect()(AddMessage);
