import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { paginate, startLoading } from '../actionCreators/messagesActionCreators';

const FORWARD = 1;
const BACKWARD = -1;

const AddMessageButton = styled(Link)`
    line-height: 1.15;
    -webkit-text-size-adjust: 100%;
    background-color: transparent;
    color: inherit;
    text-decoration: none;
    width: 80rem;
    height: 5rem;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    text-decoration: none;
    color: inherit;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`

const StyledLink = styled(Link)`
    text-decoration: none;
    color: inherit;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`

const Messages = props => {
    const [pageRange, setPageRange] = useState({ startPage: 0, endPage: 4 });
    useEffect(() => {
        props.dispatch(paginate(pageRange));
        props.dispatch(startLoading());
    }, [pageRange]);

    const nextPage = direction => pageRange => {
        const startPage = pageRange.startPage + direction * 5;
        const endPage = pageRange.endPage + direction * 5;
        if (startPage < 0) {
            return pageRange;
        }
        return { startPage, endPage };
    }

    const computeMessageClassName = (order, numOfMessages) => {
        if (numOfMessages === 1) {
            return 'only-message';
        }
        if (order === 0) {
            return 'first-message';
        }
        if (order === (numOfMessages - 1)) {
            return 'last-message';
        }
        return 'regular-message';
    }

    return (
        <div className="messages-page">
            {props.loading ? <div className="loader"></div> :
                <React.Fragment>
                    <AddMessageButton to="/add-message"><span className="normal-button">Add Message</span></AddMessageButton>
                    <div className="massages-area">
                        {!props.error ? props.messages.map((message, index) =>
                            <StyledLink key={message.id} to={`/message/${message.id}`}><div key={message.id} className={computeMessageClassName(index, props.messages.length)}>{message.text}</div></StyledLink>) : props.error}
                    </div>
                    <div className="nav-buttons">
                        <span className="normal-button" onClick={() => setPageRange(nextPage(BACKWARD))}>Prev</span>
                        <span className="normal-button" style={{ marginLeft: '1rem' }} onClick={() => setPageRange(nextPage(FORWARD))}>Next</span>
                    </div>
                </React.Fragment>
            }
        </div>
    )
};

const mapStateToProps = state => {
    return {
        messages: state.messages.messages,
        error: state.messages.error,
        loading: state.messages.loading
    }
}
export default connect(mapStateToProps)(Messages);
