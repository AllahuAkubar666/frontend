import { LOAD_MESSAGES, NO_MESSAGES, START_LOADING } from '../actions/messagesActions';

const messagesReducer = (state = { messages: [], error: undefined, loading: true }, action) => {
    switch (action.type) {
        case LOAD_MESSAGES:
            return { 
                messages: action.payload, 
                error: undefined,
                loading: false
            };
        case NO_MESSAGES:
            return { 
                messages: state.messages, 
                error: action.payload,
                loading: false
            };
        case START_LOADING:
            return {
                messages: state.message,
                error: state.error,
                loading: true
            }
        default:
            return state;
    }
};

export default messagesReducer;
