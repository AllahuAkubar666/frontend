import { PAGINATE, NO_MESSAGES, LOAD_MESSAGES, START_LOADING } from '../actions/messagesActions';

export const paginate = pageRange => ({ type: PAGINATE, payload: pageRange });

export const noMessages = error => ({ type: NO_MESSAGES, payload: error });

export const loadMessages = messages => ({ type: LOAD_MESSAGES, payload: messages });

export const startLoading = () => ({ type: START_LOADING });