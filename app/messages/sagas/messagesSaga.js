import { put, takeLatest } from 'redux-saga/effects';
import { noMessages, loadMessages } from '../actionCreators/messagesActionCreators';
import { store } from 'react-notifications-component';

import { PAGINATE } from '../actions/messagesActions';
import { BASE_URL } from '../../common/url';

function* fetchMessages(action) {
    const url = new URL(`${BASE_URL}/message`);
    url.search = new URLSearchParams(action.payload).toString();
    const messages = yield fetch(url)
        .then(response => response.json());
    if (messages.error) {
        yield put(noMessages(messages.error));
        store.addNotification({
            title: 'No messages found',
            message: messages.error,
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
                duration: 1000,
                onScreen: true
            }
        });
    } else {
        yield put(loadMessages(messages));
    }
}

function* paginate() {
    yield takeLatest(PAGINATE, fetchMessages);
}

export default paginate;
