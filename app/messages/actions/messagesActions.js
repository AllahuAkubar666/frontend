export const PAGINATE = 'PAGINATE';
export const NO_MESSAGES = 'NO_MESSAGES';
export const LOAD_MESSAGES = 'LOAD_MESSAGES';
export const START_LOADING = 'START_LOADING';