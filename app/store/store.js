import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import messagesReducer from '../messages/reducers/messagesReducer';
import paginate from '../messages/sagas/messagesSaga';

import messageReducer from '../message/reducer/messageReducer';
import message from '../message/sagas/messageSaga';

import addMessageReducer from '../addMessage/reducers/addMessageReducer';
import addMessage from '../addMessage/sagas/addMessageSaga';

function* rootSaga() {
    yield all([
        paginate(),
        message(),
        addMessage()
    ]);
}

const sagaMiddleWare = createSagaMiddleware();
const store = createStore(
    combineReducers({
        messages: messagesReducer,
        message: messageReducer,
        newMessage: addMessageReducer
    }),
    applyMiddleware(sagaMiddleWare)
);
sagaMiddleWare.run(rootSaga);

export default store;
