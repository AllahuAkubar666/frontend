const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const ESLintWebpackPlugin = require('eslint-webpack-plugin');
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');

const TARGET = process.env.npm_lifecycle_event;
const PATHS = {
    app: path.join(__dirname, 'app', 'index'),
    build: path.join(__dirname, 'build')
};

const common = {
    entry: {
        app: PATHS.app
    },
    output: {
        path: PATHS.build,
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    }
};

if (TARGET === 'start:dev' || !TARGET) {
    module.exports = merge(common, {
        mode: 'development',
        devtool: 'eval-source-map',
        devServer: {
            contentBase: path.join(__dirname, 'build'),
            historyApiFallback: true,
            hot: true,
            inline: true,
            stats: 'errors-only',
            compress: true,
            host: process.env.HOST,
            port: process.env.PORT
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new ESLintWebpackPlugin(),
            new StylelintWebpackPlugin({
                fix: true
            })
        ]
    });
}

if (TARGET === 'build') {
    module.exports = merge(common, {});
}
